const operators = ['+', '-', '*', '/', '=', 'Enter', '**'];
const operands_div = document.getElementById('operands');
const result_div = document.getElementById('result');
const number_buttons = document.querySelectorAll('.number');
const operator_buttons = document.querySelectorAll('.operator');
const clear_button = document.getElementById('clear');
var first_number = 0;
var second_number = 0;
var operation = '';

function isAllowedSymbol(evt) {
    evt = (evt) ? evt : window.event;
    let char_code = (evt.which) ? evt.which : evt.keyCode;

    if ((char_code > 47 && char_code < 58) || (char_code > 95 && char_code < 106) || (char_code === 110 || char_code === 190)) {
        return true;
    }
    return false;
}

function checkDotsCount(key) {
	if (key === '.' && operands_div.innerText.indexOf('.') > -1) return false;

	return true
}

function isOperationSymbol(key) {
	if (operators.includes(key)) return true;
	
	return false;
}

function doOperation(operation, first_number, second_number) {
	let result = 0;

	switch(operation) {
		case '+':
			result = add(first_number, second_number);
			break;
		case '-':
			result = subtract(first_number, second_number);
			break;
		case '*':
			result = multiply(first_number, second_number);
			break;
		case '/':
			result = divide(first_number, second_number);
			break;
		case '**':
			result = power(first_number, second_number);
			break;	
	}

	return result;
}

function clearDivIfOperators() {
	if (operators.indexOf(operands_div.innerText) > -1) {
		result_div.innerText = operands_div.innerText;
		operands_div.innerText = operands_div.innerText.replace(/\D/g,'');
	}
}

function checkButton(e) {
	clearDivIfOperators();

	if (checkDotsCount(e.key) && isAllowedSymbol(e) && operands_div.innerText.length < 20) {
		if ((operands_div.innerText == '0') && (e.key !== '.')) operands_div.innerText = e.key;
		else operands_div.innerText += e.key;
	}

	if (e.key === 'Backspace') {
		if (operands_div.innerText.length > 1) operands_div.innerText = operands_div.innerText.slice(0, -1);
		else operands_div.innerText = '0';
	}

	if (e.key === 'Delete') {
		clearResults();
	}

	if (isOperationSymbol(e.key)) {
		let operation_symbol = e.key === 'Enter' ? '=' : e.key;
		handleOperationPress(operation_symbol);
	}
}

function pressNumberButton() {
	clearDivIfOperators();

	if (checkDotsCount(this.innerText) && operands_div.innerText.length < 20) {
		if ((operands_div.innerText == '0') && (this.innerText !== '.'))  operands_div.innerText = this.innerText;
		else operands_div.innerText += this.innerText;
	}
}

function handleOperation(operation_symbol) {
	if (operation_symbol === '=') {
		second_number = parseFloat(operands_div.innerText);
		let result = doOperation(operation, first_number, second_number);
		result_div.innerText = result;
		first_number = (typeof result === 'string' && result.indexOf('!') > -1) ? 0 : result;
		operands_div.innerText = '';
	}
	else {
		result_div.innerText = operands_div.innerText;
		operands_div.innerText = operation_symbol;
		first_number = result_div.innerText.indexOf('!') > -1 ? 0 : parseFloat(result_div.innerText);
		operation = operation_symbol;
	}
}

function handleOperationPress(operation_symbol) {
	if (operands_div.innerText.length > 0) handleOperation(operation_symbol);
	else if (operation_symbol !== '=') {
		operation = operation_symbol;
		result_div.innerText = operation_symbol;
	}
}

function pressOperatorButton() {
	clearDivIfOperators();

	let operation_symbol = this.innerText === 'xy' ? '**' : this.innerText;
	handleOperationPress(operation_symbol);
}

function clearResults() {
	operands_div.innerText = '0';
	result_div.innerText = '';
	operation = '';
	first_number = 0;
	second_number = 0;
}

function add(a, b) {
	return a + b;
}

function subtract(a, b) {
	return a - b;
}

function multiply(a, b) {
	return a * b;
}

function divide(a, b) {
	if (b === 0) return 'Devision by 0!'

	return a / parseFloat(b);
}

function power(a, b) {
	return a ** b;
}

document.addEventListener('keydown', checkButton);
number_buttons.forEach(number_button => number_button.addEventListener('mouseup', pressNumberButton));
operator_buttons.forEach(operator_button => operator_button.addEventListener('mouseup', pressOperatorButton));
clear_button.addEventListener('mouseup', clearResults);
